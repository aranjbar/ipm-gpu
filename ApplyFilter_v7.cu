#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define err(format, ...) do { fprintf(stderr, format, ##__VA_ARGS__); exit(1); } while (0)

#ifdef __CUDACC__
inline void checkCuda(cudaError_t e) {
    if (e != cudaSuccess) {
        // cudaGetErrorString() isn't always very helpful. Look up the error
        // number in the cudaError enum in driver_types.h in the CUDA includes
        // directory for a better explanation.
        err("CUDA Error %d: %s\n", e, cudaGetErrorString(e));
    }
}

inline void checkLastCudaError() {
    checkCuda(cudaGetLastError());
}
#endif


void initimage(float**image,int nrow, int ncol);
void loadimage(char*filename,float**image,int*nrow, int*ncol);
void saveimage(char*filename,float*image,int nrow, int ncol);
void applyfilter(float*image,int nrow, int ncol,float*filter);

//Just worked for odd dimension of filter
__global__ void ApplyFilterKernel(float* imageDevice,int nrow, int ncol, float* filterDevice, int filterncol, int filternrow, float* imageOutDevice){
  int objectId = blockDim.x * blockIdx.x + threadIdx.x;
  if(objectId<((nrow*ncol)-1)){
      int CurrRow = objectId/(ncol-(2*(filterncol/2)))+ filternrow/2;
      int CurrCol = objectId%(ncol-(2*(filterncol/2))) + filterncol/2;
      int i_image,j_image, i_filter,j_filter;
      float result=0;
      
	  
	  
  } 
}

int main(int argc, char* argv[]){
   if(argc<3){
      printf("Input Error\n");
      return 1;
   }

   //char filenamein[]="0_jpeg.raw", filenameout[]="0_jpeg_v8.raw";


   char filenamein[100], filenameout[100];
   strcpy(filenamein,argv[1]);
   strcpy(filenameout,argv[2]);
   
   float*imageHost,*imageOutHost;
   int nrow, ncol;

   loadimage(filenamein,&imageHost,&nrow,&ncol);
   printf("nrow: %d, ncol: %d \n",nrow,ncol);

   initimage(&imageOutHost,nrow, ncol);

   int filterncol=3,filternrow=3;//dimensions must be odd numbers
   //float* filterHost;
   //filterHost = (float*) malloc(filterncol*filternrow*sizeof(float));
   //int count;
   //for(count=0;count<filterncol*filternrow;count++)filterHost[count] = 1.0/((float)filterncol*(float)filternrow);
   
   //sobel filter
   float filterHost[] = {-1.0,0.0,1.0,-2.0,0.0,2.0,-1.0,0.0,1.0};
   //float filterHost[] = {-1.0,-2.0,-1.0,0.0,0,0.0,1.0,2.0,1.0};
   // float filterHost[] = {-2.0,-2.0,0.0,-2.0,0.0,2.0,0.0,2.0,2.0};
   ////count =1;
   ////printf("filterHost[%d]: %f\n",count,filterHost[count]);
   float *imageDevice, *imageOutDevice, *filterDevice;

   //Allocation in GPU
   checkCuda(cudaMalloc((void**)&imageDevice, ncol*nrow*sizeof(float)));
   checkCuda(cudaMalloc((void**)&imageOutDevice, ncol*nrow*sizeof(float)));
   checkCuda(cudaMalloc((void**)&filterDevice, filterncol*filternrow*sizeof(float)));
 
   //Copy to GPU
   checkCuda(cudaMemcpy(imageDevice, imageHost, ncol*nrow*sizeof(float), cudaMemcpyHostToDevice));
   checkCuda(cudaMemcpy(imageOutDevice, imageOutHost, ncol*nrow*sizeof(float), cudaMemcpyHostToDevice));
   checkCuda(cudaMemcpy(filterDevice, filterHost, filterncol*filternrow*sizeof(float), cudaMemcpyHostToDevice));

   //Kernel Call
    unsigned int numThreadsPerClusterBlock = 256;
    unsigned int numClusterBlocks =
        (((ncol-(2*(filterncol/2)))*(nrow-(2*(filternrow/2)))) + numThreadsPerClusterBlock - 1) / numThreadsPerClusterBlock;

    printf("filternrow/2: %d \n",filternrow/2);

    printf("ncol-(2*(filterncol/2)): %d \n",ncol-(2*(filterncol/2)));
    printf("nrow-(2*(filternrow/2)): %d \n",nrow-(2*(filternrow/2)));

    printf("numThreadsPerClusterBlock: %d \n",numThreadsPerClusterBlock);
    printf("numClusterBlocks: %d \n",numClusterBlocks);

    ApplyFilterKernel<<<numClusterBlocks, numThreadsPerClusterBlock>>>(imageDevice,nrow, ncol,  filterDevice, filterncol,filternrow, imageOutDevice);
    cudaDeviceSynchronize(); checkLastCudaError();

   //Copy to CPU
   checkCuda(cudaMemcpy(imageOutHost, imageOutDevice, ncol*nrow*sizeof(float), cudaMemcpyDeviceToHost));

   //Free in GPU
   checkCuda(cudaFree(imageDevice));
   checkCuda(cudaFree(imageOutDevice));
   checkCuda(cudaFree(filterDevice));


   saveimage(filenameout,imageOutHost,nrow,ncol);

   //Free in CPU
   free(imageHost);
   free(imageOutHost);
   
   return(0);
}
void initimage(float**image,int nrow, int ncol){
    (*image) = (float*) malloc((nrow)*(ncol)*sizeof(float));
    int count;
    for(count=0;count<(nrow*ncol);count++) (*image)[count] = 0.0;
}
void loadimage(char*filename, float**image,int*nrow, int*ncol){
    FILE *fp;
   
    fp = fopen(filename, "r");
    float temp;
    fscanf(fp, "%f", &temp);
    *nrow = temp;
    fscanf(fp, "%f", &temp);
    *ncol = temp;
    (*image) = (float*) malloc((*nrow)*(*ncol)*sizeof(float));

    int i,j;
    for(i=0;i<*nrow;i++)
       for(j=0;j<*ncol;j++)
           fscanf(fp, "%f", &((*image)[i*(*ncol)+j]));

    fclose(fp);
}
void saveimage(char*filename, float*image,int nrow, int ncol){
    FILE *fp;
   
    fp = fopen(filename, "w+");
    fprintf(fp, "%f\n", (float)nrow);
    fprintf(fp, "%f\n", (float)ncol);
    int i,j;
    for(i=0;i<nrow;i++)
       for(j=0;j<ncol;j++){
           fprintf(fp, "%f", image[i*ncol+j]);
           if((i!=(nrow-1))||(j!=(ncol-1)))fprintf(fp, "\n");
       }
    fclose(fp);
}
