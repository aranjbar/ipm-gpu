//source: https://github.com/lzhengchun/matrix-cuda/blob/master/matrix_cuda.cu

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <assert.h>

#define TYPE float


/*
 Returns the current time in miliseconds.
*/
double getMilitime(){
        struct timeval ret;
        gettimeofday(&ret, NULL);
        return ((ret.tv_sec ) * 1000000u + ret.tv_usec) / 1.e6;
}



void MatrixMultiplicationCPU(TYPE*A, TYPE*B, TYPE*C, int M, int N, int K){
  int i,j,count;
  for(i=0;i<M;++i)
      for(j=0;j<K;++j)
          for(count=0;count<N;++count){
              C[i*K+j] += A[i*N+count] * B[count*K+j];
          }
}

#define err(format, ...) do { fprintf(stderr, format, ##__VA_ARGS__); exit(1); } while (0)

#ifdef __CUDACC__
inline void checkCuda(cudaError_t e) {
    if (e != cudaSuccess) {
        // cudaGetErrorString() isn't always very helpful. Look up the error
        // number in the cudaError enum in driver_types.h in the CUDA includes
        // directory for a better explanation.
        err("CUDA Error %d: %s\n", e, cudaGetErrorString(e));
    }
}

inline void checkLastCudaError() {
    checkCuda(cudaGetLastError());
}
#endif

void Allocate(TYPE**A, TYPE**B, TYPE**C, int M, int N, int K){
  (*A) = (*B) = (*C) = NULL;
  (*A) = (TYPE*)malloc(sizeof(TYPE*) * M * N);
  (*B) = (TYPE*)malloc(sizeof(TYPE*) * N * K);
  (*C) = (TYPE*)malloc(sizeof(TYPE*) * M * K);

  assert((*A) != NULL);
  assert((*B) != NULL);
  assert((*C) != NULL);
}
void Fill(TYPE*A, TYPE*B, int M, int N, int K){
  int i;
  for (i=0; i<M*N; i++) A[i]= 1.0;
  for (i=0; i<N*K; i++) B[i]= 2.0;
}
void UnAllocate(TYPE**A, TYPE**B, TYPE**C){
  free((*A));
  free((*B));
  free((*C));
}

void Print2DMatrix(TYPE*A, int M, int N) {
  int i;
  for(i = 0; i < M*N; ++i){
      if((i%M)==0) printf("\n");
      printf("%f ",A[i]);
  }
  printf("\n");
}



__global__ void MatrixMultiplication(TYPE*A_Device, TYPE*B_Device, TYPE*C_Device, int M, int N, int K){


  unsigned int objectIdX = blockDim.x * blockIdx.x + threadIdx.x;
  unsigned int objectIdY = blockDim.y * blockIdx.y + threadIdx.y;
  TYPE sum=0;
  int count;
  if (objectIdX < M && objectIdY < K) {
      for(count=0;count<N;++count){
          sum += A_Device[objectIdX*N+count] * B_Device[count*K+objectIdY];
      }
      C_Device[objectIdX*K+objectIdY]=sum;
  }
}

int main(int argc, char* argv[]){
  if(argc<4){
      printf("Input Error\n");
      return 1;
  }

  int M = atoi(argv[1]);
  int N = atoi(argv[2]);
  int K = atoi(argv[3]);
  
  TYPE*A,*B,*C, *A_Device_gpu0, *A_Device_gpu1, *B_Device_gpu0, *B_Device_gpu1, *C_Device_gpu0, *C_Device_gpu1;
  Allocate(&A,&B,&C,M,N,K);
  Fill(A,B,M,N,K);

  //Allocate in Device
  checkCuda(cudaSetDevice(0));
  checkCuda(cudaMalloc(&A_Device_gpu0, M*N/2*sizeof(TYPE)));
  checkCuda(cudaMalloc(&B_Device_gpu0, N*K*sizeof(TYPE)));
  checkCuda(cudaMalloc(&C_Device_gpu0, M/2*K*sizeof(TYPE)));

  checkCuda(cudaSetDevice(1));
  checkCuda(cudaMalloc(&A_Device_gpu1, M*N/2*sizeof(TYPE)));
  checkCuda(cudaMalloc(&B_Device_gpu1, N*K*sizeof(TYPE)));
  checkCuda(cudaMalloc(&C_Device_gpu1, M/2*K*sizeof(TYPE)));

  //Copy to Device
  checkCuda(cudaSetDevice(0));
  cudaMemcpy(A_Device_gpu0, &A[0], M*N/2*sizeof(TYPE), cudaMemcpyHostToDevice);
  cudaMemcpy(B_Device_gpu0, B, N*K*sizeof(TYPE), cudaMemcpyHostToDevice);

  checkCuda(cudaSetDevice(1));
  cudaMemcpy(A_Device_gpu1, &A[M*N/2], M*N/2*sizeof(TYPE), cudaMemcpyHostToDevice);
  cudaMemcpy(B_Device_gpu1, B, N*K*sizeof(TYPE), cudaMemcpyHostToDevice);

  dim3 dimBlock(32, 32);
  dim3 dimGrid;
  dimGrid.x = (M*N/2 + dimBlock.x - 1) / dimBlock.x;
  dimGrid.y = (N*K + dimBlock.y - 1) / dimBlock.y;

  printf("start timing\tm=%d,n=%d,k=%d\n",M,N,K);
  double start_time = getMilitime();

  checkCuda(cudaSetDevice(0));
  MatrixMultiplication<<<dimGrid,dimBlock>>>(A_Device_gpu0,B_Device_gpu0,C_Device_gpu0,M/2,N,K);

  checkCuda(cudaSetDevice(1));
  MatrixMultiplication<<<dimGrid,dimBlock>>>(A_Device_gpu1,B_Device_gpu1,C_Device_gpu1,M/2,N,K);

  cudaDeviceSynchronize(); checkLastCudaError();

  printf("elapsed time (Multi-GPU): %f sec\n", getMilitime()-start_time);

  //copy to Host
  //cudaMemcpy(C, C_Device, M*K*sizeof(TYPE), cudaMemcpyDeviceToHost);

  //Free in Device
  checkCuda(cudaSetDevice(0));
  checkCuda(cudaFree(A_Device_gpu0));
  checkCuda(cudaFree(B_Device_gpu0));
  checkCuda(cudaFree(C_Device_gpu0));

  checkCuda(cudaSetDevice(1));
  checkCuda(cudaFree(A_Device_gpu1));
  checkCuda(cudaFree(B_Device_gpu1));
  checkCuda(cudaFree(C_Device_gpu1));

  //Print2DMatrix(C,M,K);

  //free on Host
  UnAllocate(&A,&B,&C);

  return(0);
}
