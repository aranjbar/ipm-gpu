import cv2
import numpy as np
import sys

img_name = sys.argv[1]

#img_name = '0.jpeg'

img = cv2.imread(img_name, cv2.IMREAD_GRAYSCALE)

img_flatten = img.flatten()
img_flatten_temp = np.concatenate((np.array(img.shape),img_flatten))

np.savetxt(img_name.replace('.', '_')+".raw", img_flatten_temp, delimiter="\t")