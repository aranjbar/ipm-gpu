import cv2
import numpy as np
import sys

#img_name = '0_jpeg.raw'
#img_type = 'jpeg'

img_name = sys.argv[1]
img_type = sys.argv[2]

print(img_name)
img = np.loadtxt(img_name, delimiter="\t")

img_flatten = img.flatten()
nrow = int(img_flatten[0])
ncol = int(img_flatten[1])
img_flatten = np.delete(img_flatten, [0,1])

img_reshaped = img_flatten.reshape((nrow,ncol))
cv2.imwrite(img_name.replace('.', '_')+'.'+img_type, img_reshaped)

#plt.imshow(img_reshaped, cmap='gray')
#plt.show()



